#!/usr/bin/env python3
"""Fetch files from transfer.usz.ch using the liquidfiles software

Dependencies:
    * python 3
    * When using a proxy, Anorov's PySocks module must be installed
        (https://github.com/Anorov/PySocks; pip install PySocks)
      On leonhard med, this package is already installed in any
      python 3 profile module. E.g.
      >>> module load python_cpu/3.6.0
    * On leomed: This module is provided by a module
      >>> module load python_cpu/3.6.4 getliquidfiles/0.1

Usage:
    Without proxy:
    {script}

    With proxy:
    >>> ssh -D <port> -N -f lm-proxy-02
    >>> ALL_PROXY="socks5://127.0.0.1:<port>" {script}

    To control parameters, see the argument description below.

    The default behaviour is that {script} asks for the personal api key.
    You can view your api key in your liquid files account settings under "API".
    Alternatively the environment variable $API_KEY can be set,
    in which case {script} will not prompt for the key.
    Be careful when using the environment variable: it will most likely be
    recorded in .bash_history.
    This can be prevented by setting
    export HISTCONTROL=ignoreboth
    and then putting a single whitespace before "API_KEY=...". bash will not record
    any line starting with a whitespace when HISTCONTROL=ignoreboth is set.
"""

__email__ = "christian.bolliger@id.ethz.ch"
__version__ = "v 0.5"


import os
import sys
import base64
import json
import hashlib
import logging
import time
import urllib.request
import urllib.error


def download(proxy_settings: tuple,
             *,
             url: str = "https://transfer.usz.ch",
             api_key_base_64: str,
             folder: str = "/messages/inbox",
             download_path: str = "./",
             delete: bool = False,
             fs_block_size: int = 4194304
             ):
    """Downloads all attachements into download_path"""
    dl_chunk_size = 16 * fs_block_size

    # Opener based on proxy settings: Normal opener if no proxy settings are given,
    # PySocks opener if proxy settings are given:
    opener = build_opener(proxy_settings)

    # Check donwload path
    try:
        os.mkdir(download_path)
    except FileExistsError:
        pass

    list_stream = liquid_stream(url_join(url, folder),
                                api_key_base_64, opener, header="application/json")
    message_list = json.loads(list_stream.read().decode())  # send API request
    # iterate over json list containing all meta data
    for node in message_list['messages']:
        message_id = node['id']
        if not node['attachments']:
            continue
        nr_att = len(node['attachments'])
        logging.info("Nr of attachments: %d", nr_att)
        for n, att in enumerate(node['attachments'], 1):
            attachment_url = att['url']
            attachment_name = att['filename']
            logging.debug("id: %s, sender: %s, url: %s, att_url: %s",
                          message_id, node['sender'], attachment_url,
                          attachment_url)
            att_path = os.path.join(download_path, attachment_name)
            try:
                if compare_checksum(att_path, att["checksum"]):
                    logging.info("Attachement %s (message id: %s"
                                 ", attachment id: %s, %d/%d): Already downloaded",
                                 attachment_name,
                                 message_id,
                                 att['id'],
                                 n,
                                 nr_att)
                    continue
            except FileNotFoundError:
                pass
            timer_cm = log_time(display_name=attachment_name)
            with timer_cm as timer:
                download_attachement(
                    src=liquid_stream(
                        attachment_url, api_key_base_64, opener, header="*/*"),
                    dest_path=att_path,
                    update_callback=timer.update_bytes,
                    chunk_size=dl_chunk_size)
            logging.info("Downloaded attachement %s (message id: %s"
                         ", attachment id: %s, %d/%d): %s",
                         attachment_name,
                         message_id,
                         att['id'],
                         n,
                         nr_att,
                         timer_cm.status_msg())
            compare_checksum(att_path, att["checksum"])
            if delete:
                # Looks like we don't have the permission to be checked with curl
                delete_url = url_join(
                    url, "message", message_id, "delete_attachments")
                delete_attachments(delete_url, api_key_base_64)
                logging.info("Deleted attachment: %s", delete_url)


def url_join(*parts):
    return "/".join(s.strip('/') for s in parts)


def build_opener(proxy_settings):
    """
    Returns an opener based on proxy settings: Normal opener if no proxy
    settings are given, PySocks opener if proxy settings are given
    """
    if proxy_settings is None:
        return urllib.request.urlopen
    try:
        import socks
        from sockshandler import SocksiPyHandler
    except ModuleNotFoundError:
        sys.exit("module PySocks needs to be available when $ALL_PROXY is set")
    protocol, host, port = proxy_settings
    protocol_mapping = {
        "socks4": socks.SOCKS4,
        "socks5": socks.SOCKS5
    }
    try:
        opener = urllib.request.build_opener(
            SocksiPyHandler(protocol_mapping[protocol], host, port)).open

        def try_open(*args, **kwargs):
            try:
                return opener(*args, **kwargs)
            except urllib.error.URLError as crash:
                if isinstance(crash.reason, socks.ProxyConnectionError):
                    sys.exit(
                        "ERROR: Connection to proxy server refused. Is the proxy running?")
                raise crash

        return try_open
    except KeyError:
        sys.exit("ERROR: Don't know the protocol " + protocol)


def liquid_stream(url, api_key_base_64, opener, *, header):
    """Calls the url for the liquidfiles API,
    "Accept" headers have to be added after the call (different) """
    request = urllib.request.Request(
        url,
        headers={
            "Authorization": "Basic " + api_key_base_64,
            "Content-Type": "application/json",
        })
    request.add_header("Accept", header)
    try:
        return opener(request)
    except urllib.error.URLError as crash:
        sys.exit("ERROR on opening : {} {}".format(
                 ", ".join(map(repr, crash.args)), repr(crash.reason)))


def download_attachement(src, dest_path,
                         update_callback=lambda *_: None,
                         chunk_size=16 * 4194304):
    """Downloads an attachement

    :param update_callback: A callback to pass the download status to a
        status logger (signature: callback(size: int))
    """
    with open(dest_path, "wb") as att_download:
        while True:
            chunk = src.read(chunk_size)
            if not chunk:
                break
            update_callback(len(chunk))
            att_download.write(chunk)
            att_download.flush()
            os.fsync(att_download)


def compare_checksum(filename, checksum):
    download_checksum = check_sha256(filename)
    if download_checksum == checksum:
        logging.info("Checksum verified!")
        return True
    logging.warning(
        "Checksum mismatch: got %s, expected: %s",
        download_checksum, checksum)
    return False


def parse_proxy_settings(s):
    protocol, host, port = s.split(":")
    return protocol, host.lstrip("/"), int(port)


def delete_attachments(del_url, api_key_base_64):
    request = urllib.request.Request(del_url,
                                     headers={
                                         "Authorization": "Basic " + api_key_base_64,
                                         "Content-Type": "application/json",
                                         "Accept": "*/*"
                                     }
                                     )
    logging.debug(del_url)
    request.get_method = lambda: 'DELETE'
    try:
        return urllib.request.urlopen(request)
    except urllib.error.URLError as crash:
        sys.exit(" ".join((crash.characters_written, crash.args, crash.reason)))


def check_sha256(filename, block_size=65536):
    hash_sha256 = hashlib.sha256()
    with open(filename, "rb") as f:
        for block in iter(lambda: f.read(block_size), b''):
            hash_sha256.update(block)
    return hash_sha256.hexdigest()


def log_time(*, level=logging.INFO, logger=logging.getLogger(), **kwargs):
    """Returns either a trivial context manager (doing nothing) or a
    DownloadProgressLogger based on the loglevel."""
    if level >= logger.getEffectiveLevel():
        return DownloadProgressLogger(**kwargs)

    class TrivialTimer:
        def __init__(self, **_):
            self.time = None

        def __enter__(self):
            pass

        def __exit__(self, *_):
            pass

        def update_bytes(self, n_bytes):
            pass

    return TrivialTimer()


class DownloadProgressLogger:
    """Context manager which prints download statistics onto stdout"""

    def __init__(self, display_name=""):
        self.t0 = None
        self.time = None
        self.n_bytes = 0
        self.display_name = display_name

    def update_bytes(self, n_bytes):
        self.time_it()
        self.n_bytes += n_bytes
        self.print_status()

    def __enter__(self):
        self.t0 = time.clock()
        return self

    def time_it(self):
        self.time = time.clock() - self.t0

    def __exit__(self, *_):
        self.time_it()
        sys.stdout.write("\r")

    def status_msg(self):
        return "\r{}: {} B, {:.2f} s, {:.2f} MB/s".format(
            self.display_name,
            self.n_bytes,
            self.time,
            self.n_bytes / (1024 ** 2 * self.time))

    def print_status(self):
        sys.stdout.write(self.status_msg())


def argparser_by_signature(f, parameter_doc=None,
                           omit=frozenset(), **kwargs):
    """Builds an argparser according to the signature of a function f.
    Type hint annotations are used to infer the type for argparse"""
    import argparse
    import inspect
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        **kwargs)
    sig = inspect.signature(f)
    if parameter_doc is None:
        parameter_doc = {}
    for p_name, p in sig.parameters.items():
        if p_name in omit:
            continue
        default = p.default
        args = {}
        if p.default is inspect.Signature.empty:
            args["required"] = True
        else:
            args["default"] = default
        if p.annotation is bool:
            args["nargs"] = '?'
            args["const"] = True
            args["default"] = args.get("default", False)
        parser.add_argument(
            "--" + p_name,
            type=p.annotation,
            help=parameter_doc.get(p_name),
            **args
        )
    return parser


if __name__ == "__main__":
    from getpass import getpass
    _proxy_settings = os.environ.get('ALL_PROXY')
    if _proxy_settings is not None:
        _proxy_settings = parse_proxy_settings(_proxy_settings)
    # Read environment variables
    logging.basicConfig(format='[%(levelname)s] %(message)s',
                        level=logging.INFO)
    cmd_args = vars(argparser_by_signature(
        f=download, description=__doc__.format(script=sys.argv[0]),
        omit={"proxy_settings", "api_key_base_64"},
        parameter_doc={"url": "Target URL",
                       "folder": "Folder to look for messages",
                       "download_path": "Folder to store downloaded files",
                       "delete": "Delete attachements after download",
                       "fs_block_size": "Chunk size for fetching attachements"
                       }).parse_args())
    API_KEY = os.environ.get('API_KEY')
    if API_KEY is None:
        API_KEY = getpass(prompt="Please enter your api key:")
    API_KEY_BASE_64 = base64.b64encode(
        bytes(API_KEY, "utf-8") + b":x").decode()
    download(_proxy_settings, api_key_base_64=API_KEY_BASE_64, **cmd_args)
