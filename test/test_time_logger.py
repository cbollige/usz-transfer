import unittest
import logging

from getliquidfiles import DownloadProgressLogger, log_time


class TestTimeLogger(unittest.TestCase):
    def test_time_logger(self):
        timer_cm = DownloadProgressLogger("test")
        timer_cm.print_status = lambda *_: None
        with timer_cm as timer:
            for _ in range(10000):
                timer.update_bytes(1)
        self.assertTrue(timer.time is not None)

    def test_log_time(self):
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        timer = log_time(level=logging.INFO, logger=logger)
        self.assertTrue(isinstance(timer, DownloadProgressLogger))
        timer = log_time(level=logging.DEBUG, logger=logger)
        self.assertFalse(isinstance(timer, DownloadProgressLogger))
